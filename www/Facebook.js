cordova.define("cordova/plugin/Facebook", function(require, exports, module) {
    var exec = require("cordova/exec");

    function Facebook() {
        this.successCallback = null;
        this.errorCallback = null;
    }

    Facebook.prototype.loginNative = function()
    {
        console.log("-----> loginNative");
        exec(null, null, "Facebook", "login", []);
    };

    /**
     * Assign either title or image to the right navigation bar button, and assign the tap callback
    */
    Facebook.prototype.login = function(successCallback, errorCallback)
    {
        console.log("-----> Login with 2 callbacks");
        this.successCallback = successCallback;
        this.errorCallback = errorCallback;
        this.loginNative();
    };

    /**
     * Internal function called by the plugin
     */
    Facebook.prototype.execDone = function(success, options)
    {
        console.log("-----> execDone: " + success);
        if (success) {
            if(typeof(this.successCallback) === "function") {
                this.successCallback(options);
            }
        } else {
            if(typeof(this.errorCallback) === "function") {
                this.errorCallback(options);
            }
        }
    };

    module.exports = new Facebook();
});
