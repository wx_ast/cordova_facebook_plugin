/*
 Facebook.h
 */
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// For older versions of Cordova, you may have to use: #import "CDVPlugin.h"
#import <Cordova/CDVPlugin.h>

@interface CDVFacebook : CDVPlugin

- (void)login:(CDVInvokedUrlCommand*)command;

- (void)success:(NSError*)error;
- (void)success:(NSString*)user_id token:(NSString*)token;


@end
