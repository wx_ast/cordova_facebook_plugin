/*
 Facebook.m
 */

#import <objc/runtime.h>
#import "CDVFacebook.h"
#import <Accounts/Accounts.h>


// For older versions of Cordova, you may have to use: #import "CDVDebug.h"
#import <Cordova/CDVDebug.h>

@implementation CDVFacebook

- (void)login:(CDVInvokedUrlCommand*)command
{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    NSString *fb_appid = @"169282889923190";
    NSDictionary *dict = @{ACFacebookAppIdKey : fb_appid, ACFacebookPermissionsKey : @[@"email"], ACFacebookAudienceKey : ACFacebookAudienceEveryone};
    [accountStore requestAccessToAccountsWithType:accountType options:dict
                                  completion:^(BOOL granted, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (granted) {
                NSLog(@"ok");
                NSArray *accounts = [accountStore accountsWithAccountType:accountType];
                ACAccount *facebookAccount = [accounts objectAtIndex:0];
                ACAccountCredential *fbCredential = [facebookAccount credential];
                NSString *accessToken = [fbCredential oauthToken];
                 [self success:[facebookAccount username] token:accessToken];
                NSLog(@"name: %@    token: %@", [facebookAccount username], accessToken);
            } else {
                [self fail:error];
                NSLog(@"error: %@        code:%d", error.localizedDescription, error.code);
            }
        });
    }];
}

- (void)success:(NSString*)user_id token:(NSString*)token
{
    NSLog(@"+success");
    NSString * jsCallBack =  [NSString stringWithFormat:@"cordova.require('cordova/plugin/Facebook').execDone(true, {user_id: '%@', token: '%@'});", user_id, token];
    [self writeJavascript:jsCallBack];
}

- (void)fail:(NSError*)error
{
    NSLog(@"+fail");
    NSString * jsCallBack = [NSString stringWithFormat:@"cordova.require('cordova/plugin/Facebook').execDone(false, {error: '%@', code: %d});", [error localizedDescription], error.code];
    [self writeJavascript:jsCallBack];
}

@end
